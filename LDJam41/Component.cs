﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41
{
    public class Component : IComponent
    {
        public Entity Owner { get; set; }
        public bool IsKilled { get; set; }

        public Component(Entity owner)
        {
            Owner = owner;
            IsKilled = false;
        }

        public virtual void Kill()
        {
            Owner.RemoveComponent(this);
        }

        public virtual void Update(float delta)
        {

        }

        public virtual void Update(float delta, EntityManager entityManager)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}
