﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Health : Component
    {
        public int MaxHp;
        public int Hp;

        public Health(Entity owner, int maxHp) : base(owner)
        {
            MaxHp = maxHp;
            Hp = maxHp;
        }

        public Health(Entity owner, int maxHp, int hp) : base(owner)
        {
            MaxHp = maxHp;
            Hp = hp;
        }

        public override void Update(float delta, EntityManager entityManager)
        {
            if(Hp <= 0)
            {
                entityManager.RemoveEntity(Owner);
            }
        }
    }
}
