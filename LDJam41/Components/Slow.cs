﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Slow : Status
    {
        private float slow;

        public Slow(Entity owner, int duration, float slow) : base(owner, duration)
        {
            this.slow = slow;
            Pathfinding pathfinding = Owner.GetComponent<Pathfinding>();
            if (pathfinding != null)
            {
                pathfinding.Speed -= slow;
            }
        }

        public override void Kill()
        {
            Pathfinding pathfinding = Owner.GetComponent<Pathfinding>();
            if (pathfinding != null)
            {
                pathfinding.Speed = pathfinding.Speed + slow;
            }
            base.Kill();
        }

        public override void Update(float delta)
        {
            if (i == timer)
            {
                if (Duration <= 0)
                {
                    Kill();
                }
                Duration--;
                i = 0;
            }
            base.Update(delta);
        }
    }
}
