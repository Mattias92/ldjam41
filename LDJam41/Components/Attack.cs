﻿using LDJam41.Entites;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Attack : Component
    {
        public int Damage { get; set; }
        public int AttackSpeed { get; set; }
        private int currentDelay;
        private Type type;

        public Attack(Entity owner, int damage, int attackSpeed, Type type) : base(owner)
        {
            Damage = damage;
            AttackSpeed = attackSpeed;
            currentDelay = 0;
            this.type = type;
        }

        public override void Update(float delta, EntityManager entityManager)
        {
            if (currentDelay == 0)
            {
                for(int i = 0; i < entityManager.Entities.Count; i++)
                {
                    if (typeof(Monster).IsAssignableFrom(entityManager.Entities[i].GetType()))
                    {
                        Sprite tSprite = Owner.GetComponent<Sprite>();
                        Sprite mSprite = entityManager.Entities[i].GetComponent<Sprite>();
                        if (tSprite != null && mSprite != null)
                        {
                            Vector2 relativePosition = tSprite.Center - mSprite.Center;
                            float distanceBetweenCenters = relativePosition.Length();
                            if (distanceBetweenCenters <= tSprite.Radius + mSprite.Radius)
                            {
                                Tower tower = (Tower)Owner;
                                entityManager.AddBullet(tSprite.Center, mSprite.Center, Damage, tower.Level, tower.GetComponent<Sprite>().Radius, type);
                                currentDelay = AttackSpeed;
                                break;
                            }
                        }
                    }
                }
            }

            if(currentDelay > 0)
            {
                currentDelay--;
            }
        }
    }
}
