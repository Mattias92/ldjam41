﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Sprite : Component
    {
        public Texture2D Texture    { get; set; }
        public Vector2 Position     { get; set; }
        public Color Color          { get; set; }
        public int Width            { get; set; }
        public int Height           { get; set; }
        public float Radius         { get; set; }
        public float Rotation       { get; set; }
        public float Order          { get; set; }

        public Sprite(Entity owner, Main main, string texture, Point position, int width, int height, float radius) : base(owner)
        {
            Texture = main.Content.Load<Texture2D>(texture);
            Position = new Vector2(position.X * width, position.Y * height);
            Color = Color.White;
            Width = width;
            Height = height;
            Radius = radius;
            Rotation = 0f;
            Order = 0.5f;
        }

        public Sprite(Entity owner, Main main, string texture, Vector2 position, int width, int height, float radius) : base(owner)
        {
            Texture = main.Content.Load<Texture2D>(texture);
            Position = position;
            Color = Color.White;
            Width = width;
            Height = height;
            Radius = radius;
            Rotation = 0f;
            Order = 0.5f;
        }

        public Vector2 Center
        {
            get
            {
                return Position + new Vector2(Width / 2, Height / 2); //Not correct?
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, new Rectangle((int)Position.X, (int)Position.Y, Width, Height), new Rectangle(0, 0, Width, Height), Color, Rotation, Vector2.Zero, SpriteEffects.None, Order);
        }
    }
}
