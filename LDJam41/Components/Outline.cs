﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Outline : Component
    {
        private Texture2D outline;
        private Vector2 position;

        public Outline(Entity owner, GraphicsDevice graphicsDevice) : base(owner)
        {
            Texture2D tex = Owner.GetComponent<Sprite>().Texture;
            position = Owner.GetComponent<Sprite>().Position;
            outline = new Texture2D(graphicsDevice, tex.Width + 4, tex.Height + 4);
            Color[] texColors = new Color[tex.Width * tex.Height];
            Color[] outColors = new Color[outline.Width * outline.Height];
            tex.GetData(texColors);

            for (int y = 0; y < tex.Height; y++)
            {
                for (int x = 0; x < tex.Width; x++)
                {
                    float texAlpha = texColors[x + y * tex.Width].A / 255f;

                    float topLeft = outColors[x + 0 + (y + 0) * outline.Width].A / 255f;
                    outColors[x + 0 + (y + 0) * outline.Width] = Color.Yellow * (texAlpha + topLeft);

                    float topRight = outColors[x + 4 + (y + 0) * outline.Width].A / 255f;
                    outColors[x + 4 + (y + 0) * outline.Width] = Color.Yellow * (texAlpha + topRight);

                    float botLeft = outColors[x + 4 + (y + 4) * outline.Width].A / 255f;
                    outColors[x + 4 + (y + 4) * outline.Width] = Color.Yellow * (texAlpha + botLeft);

                    float botRight = outColors[x + 0 + (y + 4) * outline.Width].A / 255;
                    outColors[x + 0 + (y + 4) * outline.Width] = Color.Yellow * (texAlpha + botRight);
                }
            }

            outline.SetData(outColors);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(outline, new Rectangle((int)position.X - 4, (int)position.Y - 4, 72, 136), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.3f);
        }
    }
}
