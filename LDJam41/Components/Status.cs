﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Status : Component
    {
        public int Duration { get; set; }
        protected int timer;
        protected int i;

        public Status(Entity owner, int duration) : base(owner)
        {
            Duration = duration;
            timer = 60;
            i = 0;
        }

        public override void Update(float delta)
        {
            i++;
            base.Update(delta);
        }
    }
}
