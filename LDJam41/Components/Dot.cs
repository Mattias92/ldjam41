﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Dot : Status
    {
        private Health health;
        private int damage;

        public Dot(Entity owner, int duration, int damage) : base(owner, duration)
        {
            health = owner.GetComponent<Health>();
            this.damage = damage;
        }

        public override void Update(float delta)
        {
            if (i == timer)
            {
                if(health != null)
                {
                    health.Hp -= damage;
                }

                if (Duration <= 0)
                {
                    Kill();
                }
                Duration--;
                i = 0;
            }
            base.Update(delta);
        }
    }
}
