﻿using LDJam41.Entites;
using LDJam41.Entites.Bullets;
using LDJam41.Entites.Monsters;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Projectile : Component
    {
        private int damage;
        private Vector2 direction;
        private float speed;
        private float elapsed;
        private Type type;
        private int level;
        private Vector2 start;
        private float radius;

        public Projectile(Entity owner, Vector2 start, Vector2 target, int damage, int level, float radius, Type type) : base(owner)
        {
            speed = 100f;
            elapsed = 0.10f;
            direction = Vector2.Normalize(target - start);
            this.damage = damage;
            this.type = type;
            this.level = level;
            this.start = start;
            this.radius = radius;
        }

        public bool ProjectileHit(EntityManager entityManager)
        {
            for (int i = 0; i < entityManager.Entities.Count; i++)
            {
                if (typeof(Monster).IsAssignableFrom(entityManager.Entities[i].GetType()))
                {
                    Sprite sprite = Owner.GetComponent<Sprite>();
                    Sprite mSprite = entityManager.Entities[i].GetComponent<Sprite>();
                    if (mSprite != null)
                    {
                        Vector2 relativePosition = sprite.Center - mSprite.Center;
                        float distanceBetweenCenters = relativePosition.Length();
                        if (distanceBetweenCenters <= sprite.Radius + mSprite.Radius)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public List<Monster> HitTargets(EntityManager entityManager)
        {
            List<Monster> hits = new List<Monster>();

            for (int i = 0; i < entityManager.Entities.Count; i++)
            {
                if (typeof(Monster).IsAssignableFrom(entityManager.Entities[i].GetType()))
                {
                    Sprite sprite = Owner.GetComponent<Sprite>();
                    Sprite mSprite = entityManager.Entities[i].GetComponent<Sprite>();
                    if (mSprite != null)
                    {
                        Vector2 relativePosition = sprite.Center - mSprite.Center;
                        float distanceBetweenCenters = relativePosition.Length();
                        if (distanceBetweenCenters <= sprite.Radius + mSprite.Radius)
                        {
                            hits.Add(entityManager.Entities[i] as Monster);
                        }
                    }
                }
            }
            return hits;
        }

        public override void Update(float delta, EntityManager entityManager)
        {
            Sprite sprite = Owner.GetComponent<Sprite>();
            if (sprite != null)
            {
                sprite.Rotation = (float)Math.Atan2(direction.X, -direction.Y);
                sprite.Position += direction * speed * elapsed;
            }

            Vector2 relativePosition = sprite.Center - start;
            float distanceBetweenCenters = relativePosition.Length();
            if (distanceBetweenCenters > sprite.Radius + radius)
            {
                entityManager.RemoveEntity(Owner);
            }

            bool hasHit = ProjectileHit(entityManager);
            if(hasHit)
            {
                entityManager.RemoveEntity(Owner);
                List<Monster> hits = HitTargets(entityManager);
                foreach(Monster m in hits)
                {
                    Health health = m.GetComponent<Health>();
                    if (health != null)
                    {
                        if (type == typeof(Arrow))
                        {
                            health.Hp -= damage * level;
                            break;
                        }
                        else if (type == typeof(Cannonball))
                        {
                            health.Hp -= damage * level;
                        }
                        else if (type == typeof(Snowball))
                        {
                            health.Hp -= damage * level;
                            if (m.GetType() != typeof(StatusImmuneMonster) && m.GetType() != typeof(StatusImmuneBoss))
                            {
                                if (!m.HasComponent<Slow>())
                                {
                                    m.AddComponent(new Slow(m, 6, 0.5f));
                                }
                            }
                        }
                        else if (type == typeof(Thunderbolt))
                        {
                            health.Hp -= damage * level;
                            if (m.GetType() != typeof(StatusImmuneMonster) && m.GetType() != typeof(StatusImmuneBoss))
                            {
                                if (!m.HasComponent<Stun>())
                                {
                                    m.AddComponent(new Stun(m, 6 + level));
                                }
                            }
                        }
                        else if (type == typeof(Fireball))
                        {
                            health.Hp -= damage;
                            if (m.GetType() != typeof(StatusImmuneMonster) && m.GetType() != typeof(StatusImmuneBoss))
                            {
                                if (!m.HasComponent<Dot>())
                                {
                                    m.AddComponent(new Dot(m, 5 + level, damage * level));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
