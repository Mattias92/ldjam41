﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Stun : Status
    {
        private float speed;

        public Stun(Entity owner, int duration) : base(owner, duration)
        {
            Pathfinding pathfinding = Owner.GetComponent<Pathfinding>();
            speed = pathfinding.Speed;
            if (pathfinding != null)
            {
                pathfinding.Speed = 0;
            }
        }

        public override void Kill()
        {
            Pathfinding pathfinding = Owner.GetComponent<Pathfinding>();
            if (pathfinding != null)
            {
                pathfinding.Speed = speed;
            }
            base.Kill();
        }

        public override void Update(float delta)
        {
            if (i == timer)
            {
                if (Duration <= 0)
                {
                    Kill();
                }
                Duration--;
                i = 0;
            }
            base.Update(delta);
        }
    }
}

