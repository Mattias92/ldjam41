﻿using LDJam41.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Components
{
    public class Pathfinding : Component
    {
        public float Speed { get; set; }
        private List<Tile> Path;

        private int i;
        private Vector2 curr;
        private Vector2 next;

        public Pathfinding(Entity owner, List<Tile> path, float speed) : base(owner)
        {
            Speed = speed;
            Path = path;
            i = 1;
            curr = Path[0].ToVector2();
            next = Path[1].ToVector2();
        }

        public override void Update(float delta, EntityManager entityManager)
        {
            Sprite sprite = Owner.GetComponent<Sprite>();           
            if (!sprite.Position.Equals(next))
            {
                if (next.X > curr.X)
                {
                    sprite.Position += new Vector2(Speed, 0f);             
                }
                else if(curr.X > next.X)
                {
                    sprite.Position += new Vector2(-Speed, 0f);
                }
                else if (next.Y > curr.Y)
                {
                    sprite.Position += new Vector2(0f, Speed);
                }
                else if (curr.Y > next.Y)
                {
                    sprite.Position += new Vector2(0f, -Speed);
                }
            }
            else
            {
                if(i != Path.Count)
                {
                    curr = next;
                    next = Path[i++].ToVector2();
                }
                else
                {
                    entityManager.MonsterPassed();
                    entityManager.RemoveEntity(Owner);
                }
            }
        }
    }
}
