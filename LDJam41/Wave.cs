﻿using LDJam41.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41
{
    public class Wave
    {
        public int Amount;
        public int Frequency;
        public int Level;
        public List<Type> Types;
        public bool Boss;

        public Wave(int amount, int frequency, int level, List<Type> types, bool boss)
        {
            Amount = amount;
            Frequency = frequency;
            Level = level;
            Types = types;
            Boss = boss;
        }
    }
}
