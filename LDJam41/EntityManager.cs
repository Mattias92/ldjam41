﻿using LDJam41.Components;
using LDJam41.Entites;
using LDJam41.Entites.Bullets;
using LDJam41.Entites.Monsters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41
{
    public class EntityManager
    {
        readonly Main main;
        public int EntityCounter { get; set; }
        public List<Entity> Entities { get; set; }

        private int i;
        public int MonstersAlive;
        public bool OngoingWave;
        public int WaveMonstersLeft;
        private int frequncy;
        private List<Type> types;
        private int level;
        private bool boss;

        public EntityManager(Main main)
        {
            this.main = main;
            EntityCounter = 1;
            Entities = new List<Entity>();
            i = 0;
            MonstersAlive = 0;
            WaveMonstersLeft = 0;
            frequncy = 0;
            OngoingWave = false;
        }

        public void AddMonster()
        {
            Random rnd = new Random();
            int r = rnd.Next(types.Count);
            Type type = types[r];

            if (type == typeof(BasicMonster))
            {
                BasicMonster monster = new BasicMonster(main, EntityCounter++, level);
                Entities.Add(monster);
                MonstersAlive++;
            }
            else if(type == typeof(BasicBoss))
            {
                BasicBoss monster = new BasicBoss(main, EntityCounter++, level);
                Entities.Add(monster);
                MonstersAlive++;
            }
            else if (type == typeof(FastMonster))
            {
                FastMonster monster = new FastMonster(main, EntityCounter++, level);
                Entities.Add(monster);
                MonstersAlive++;
            }
            else if (type == typeof(FastBoss))
            {
                FastBoss monster = new FastBoss(main, EntityCounter++, level);
                Entities.Add(monster);
                MonstersAlive++;
            }
            else if (type == typeof(ArmorMonster))
            {
                ArmorMonster monster = new ArmorMonster(main, EntityCounter++, level);
                Entities.Add(monster);
                MonstersAlive++;
            }
            else if (type == typeof(ArmorBoss))
            {
                ArmorBoss monster = new ArmorBoss(main, EntityCounter++, level);
                Entities.Add(monster);
                MonstersAlive++;
            }
            else if (type == typeof(StatusImmuneMonster))
            {
                StatusImmuneMonster monster = new StatusImmuneMonster(main, EntityCounter++, level);
                Entities.Add(monster);
                MonstersAlive++;
            }
            else if (type == typeof(StatusImmuneBoss))
            {
                StatusImmuneBoss monster = new StatusImmuneBoss(main, EntityCounter++, level);
                Entities.Add(monster);
                MonstersAlive++;
            }
        }

        public void AddMonsterWave(Wave wave)
        {
            WaveMonstersLeft = wave.Amount;
            frequncy = wave.Frequency;
            types = wave.Types;
            level = wave.Level;
            OngoingWave = true;
            boss = wave.Boss;
        }

        public void AddTower(Tower tower)
        {
            Entities.Add(tower);
        }

        public void AddBullet(Vector2 start, Vector2 target, int damage, int level, float radius, Type type)
        {
            if (type == typeof(Arrow))
            {
                Arrow bullet = new Arrow(main, EntityCounter++, start, target, damage, level, radius);
                Entities.Add(bullet);
            }
            else if (type == typeof(Fireball))
            {
                Fireball bullet = new Fireball(main, EntityCounter++, start, target, damage, level, radius);
                Entities.Add(bullet);
            }
            else if (type == typeof(Thunderbolt))
            {
                Thunderbolt bullet = new Thunderbolt(main, EntityCounter++, start, target, damage, level, radius);
                Entities.Add(bullet);
            }
            else if (type == typeof(Snowball))
            {
                Snowball bullet = new Snowball(main, EntityCounter++, start, target, damage, level, radius);
                Entities.Add(bullet);
            }
            else if (type == typeof(Cannonball))
            {
                Cannonball bullet = new Cannonball(main, EntityCounter++, start, target, damage, level, radius);
                Entities.Add(bullet);
            }
        }

        public void AddCard(Card card)
        {
            Entities.Add(card);
        }

        public void AddButton(Vector2 vector)
        {
            Button button = new Button(EntityCounter++);
            button.AddComponent(new Sprite(button, main, "nextwave", vector, 64, 32, 0f));
            Entities.Add(button);
        }

        public void MonsterPassed()
        {
            main.Lives--;
        }

        public void RemoveEntity(Entity entity)
        {
            Entities.Remove(entity);
            if(typeof(Monster).IsAssignableFrom(entity.GetType()))
            {
                MonstersAlive--;
            }
        }

        public int InBounds(Vector2 vector)
        {
            for (int i = 0; i < Entities.Count; i++)
            {
                if(Entities[i].InBounds(vector) > 0)
                {
                    return Entities[i].InBounds(vector);
                }
            }
            return 0;
        }

        public int InBoundsIgnoreType(Vector2 vector, Type type)
        {
            for (int i = 0; i < Entities.Count; i++)
            {
                if (Entities[i].InBoundsIgnoreType(vector, type) > 0)
                {
                    return Entities[i].InBoundsIgnoreType(vector, type);
                }
            }
            return 0;
        }

        public Entity GetEntityById(int id)
        {
            return Entities.First(entity => entity.Id == id);
        }

        public void Update(float delta)
        {
            if(WaveMonstersLeft > 0)
            {
                if(i == 0)
                {
                    AddMonster();
                    WaveMonstersLeft--;
                    i++;
                }
                else
                {
                    i++;
                    if (i >= frequncy)
                    {
                        i = 0;
                    }
                }            
            }

            if(OngoingWave && MonstersAlive == 0 && WaveMonstersLeft == 0)
            {
                OngoingWave = false;
                main.EndWave(boss);
                main.Wave++;
            }

            for(int i = 0; i < Entities.Count; i++)
            {
                Entities[i].Update(delta);
                Entities[i].Update(delta, this);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Entity e in Entities)
            {
                e.Draw(spriteBatch);
            }
        }
    }
}
