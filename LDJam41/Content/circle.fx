float2 circleCenter = { 0.5, 0.5 }; 
float circleRadius = 0.4; 
float spriteSize = 32;

float4 PixelShaderFunction(float4 color : COLOR0, float2 uv : TEXCOORD0) : COLOR0 
{ 
    float distanceFromCenter = length(circleCenter - uv); 
    float distanceFromCircle = abs(circleRadius - distanceFromCenter) * spriteSize; 

    float alpha = (distanceFromCircle < 0.5) ? 1 : 0;

    return color * alpha; 
}

technique Technique1 
{ 
    pass Pass1 
    { 
        PixelShader = compile ps_2_0 PixelShaderFunction(); 
    } 
} 