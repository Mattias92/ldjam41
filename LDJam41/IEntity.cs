﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41
{
    public interface IEntity
    {
        void AddComponent(Component component);
        void RemoveComponent(Component component);
        T GetComponent<T>() where T : Component;
        bool HasComponent<T>() where T : Component;
    }
}
