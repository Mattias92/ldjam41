﻿using LDJam41.Components;
using LDJam41.Entites.Bullets;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Towers
{
    public class FrostTower : Tower
    {
        private SpriteFont font;

        public FrostTower(Main main, int id, Point point) : base(id)
        {
            AddComponent(new Sprite(this, main, "frosttower", point, 32, 32, 32f));
            AddComponent(new Attack(this, 1, 10, typeof(Snowball)));
            DamagePerLevel = 0;
            font = main.font;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.DrawString(font, Level.ToString(), GetComponent<Sprite>().Position, Color.Red, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.5f);
        }
    }
}
