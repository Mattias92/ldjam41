﻿using LDJam41.Components;
using LDJam41.Entites.Bullets;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace LDJam41.Entites.Towers
{
    public class BasicTower : Tower
    {
        private SpriteFont font;

        public BasicTower(Main main, int id, Point point) : base(id)
        {
            AddComponent(new Sprite(this, main, "tower", point, 32, 32, 150f));
            AddComponent(new Attack(this, 5, 25, typeof(Arrow)));
            DamagePerLevel = 4;
            font = main.font;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.DrawString(font, Level.ToString(), GetComponent<Sprite>().Position, Color.Red, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.5f);
        }
    }
}
