﻿using LDJam41.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Bullets
{
    public class Cannonball : Bullet
    {
        public Cannonball(Main main, int id, Vector2 start, Vector2 end, int damage, int level, float radius) : base(id)
        {
            AddComponent(new Sprite(this, main, "cannonball", start, 9, 9, 8f));
            AddComponent(new Projectile(this, start, end, damage, level, radius, GetType()));
        }
    }
}
