﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Cards
{
    public class CardFlameTower : Card
    {
        public CardFlameTower(Main main, int id) : base(main, id)
        {
            Name = "Flame Tower";
            Cost = 2;
            Texture = "CardFlameTower";
        }
    }
}
