﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using LDJam41.Components;

namespace LDJam41.Entites.Cards
{
    class CardBasicTower : Card
    {
        public CardBasicTower(Main main, int id) : base(main, id)
        {
            Name = "Basic Tower";
            Cost = 1;
            Texture = "CardBasicTower";
        }

        public override void Play()
        {
            
        }
    }
}
