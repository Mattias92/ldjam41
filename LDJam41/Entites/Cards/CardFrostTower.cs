﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Cards
{
    public class CardFrostTower : Card
    {
        public CardFrostTower(Main main, int id) : base(main, id)
        {
            Name = "Frost Tower";
            Cost = 2;
            Texture = "CardFrostTower";
        }
    }
}
