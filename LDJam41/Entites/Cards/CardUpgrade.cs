﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Cards
{
    public class CardUpgrade : Card
    {
        public CardUpgrade(Main main, int id) : base(main, id)
        {
            Name = "Upgrade";
            Cost = 2;
            Texture = "CardUpgrade";
        }
    }
}
