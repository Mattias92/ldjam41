﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Cards
{
    public class CardUpgradePlus : Card
    {
        public CardUpgradePlus(Main main, int id) : base(main, id)
        {
            Name = "Upgrade Plus";
            Cost = 3;
            Texture = "CardUpgradePlus";
        }
    }
}
