﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Cards
{
    public class CardThunderTower : Card
    {
        public CardThunderTower(Main main, int id) : base(main, id)
        {
            Name = "Thunder Tower";
            Cost = 2;
            Texture = "CardThunderTower";
        }
    }
}
