﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Cards
{
    public class CardCannonTower : Card
    {
        public CardCannonTower(Main main, int id) : base(main, id)
        {
            Name = "Cannon Tower";
            Cost = 1;
            Texture = "CardCannonTower";
        }
    }
}
