﻿using LDJam41.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites
{
    public class Button : Entity
    {
        public Button(int id) : base(id)
        {

        }

        public override int InBounds(Vector2 vector)
        {
            Sprite sprite = GetComponent<Sprite>();
            Rectangle bounds = new Rectangle((int)sprite.Position.X, (int)sprite.Position.Y, sprite.Width, sprite.Height);
            if (bounds.Contains(vector))
            {
                return Id;
            }
            return 0;
        }

        public override int InBoundsIgnoreType(Vector2 vector, Type type)
        {
            if (!type.IsAssignableFrom(GetType()))
            {
                Sprite sprite = GetComponent<Sprite>();
                Rectangle bounds = new Rectangle((int)sprite.Position.X, (int)sprite.Position.Y, sprite.Width, sprite.Height);
                if (bounds.Contains(vector))
                {
                    return Id;
                }
            }
            return 0;
        }
    }
}
