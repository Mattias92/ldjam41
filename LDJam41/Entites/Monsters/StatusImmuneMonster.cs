﻿using LDJam41.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Monsters
{
    public class StatusImmuneMonster : Monster
    {
        public StatusImmuneMonster(Main main, int id, int level) : base(id, level)
        {
            AddComponent(new Sprite(this, main, "statusimmunemonster", main.Gameboard.Path[0].ToPoint(), 32, 32, 8f));
            AddComponent(new Pathfinding(this, main.Gameboard.Path, 1f));
            AddComponent(new Health(this, 30 * level));
        }
    }
}
