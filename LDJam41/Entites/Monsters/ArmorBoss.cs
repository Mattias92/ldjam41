﻿using LDJam41.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites.Monsters
{
    public class ArmorBoss : Monster
    {
        public ArmorBoss(Main main, int id, int level) : base(id, level)
        {
            AddComponent(new Sprite(this, main, "armorboss", main.Gameboard.Path[0].ToPoint(), 32, 32, 16f));
            AddComponent(new Pathfinding(this, main.Gameboard.Path, 0.5f));
            AddComponent(new Health(this, 3000 * level));
        }
    }
}
