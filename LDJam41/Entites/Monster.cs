﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites
{
    public class Monster : Entity
    {
        public int Level;

        public Monster(int id, int level) : base(id)
        {
            Level = level;
        }
    }
}
