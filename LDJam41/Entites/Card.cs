﻿using LDJam41.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites
{
    public class Card : Entity
    {
        public string Name      { get; set; }
        public int Cost         { get; set; }
        public bool IsSelected  { get; set; }
        public string Texture   { get; set; }

        public Card(Main main, int id) : base(id)
        {
            IsSelected = false;
        }

        public override int InBounds(Vector2 vector)
        {
            Sprite sprite = GetComponent<Sprite>();
            Rectangle bounds = new Rectangle((int)sprite.Position.X, (int)sprite.Position.Y, sprite.Width, sprite.Height);
            if(bounds.Contains(vector))
            {
                return Id;
            }
            return 0;
        }

        public override int InBoundsIgnoreType(Vector2 vector, Type type)
        {
            if(!type.IsAssignableFrom(GetType()))
            {
                Sprite sprite = GetComponent<Sprite>();
                Rectangle bounds = new Rectangle((int)sprite.Position.X, (int)sprite.Position.Y, sprite.Width, sprite.Height);
                if (bounds.Contains(vector))
                {
                    return Id;
                }
            }
            return 0;
        }

        public virtual void Play()
        {
            
        }
    }
}
