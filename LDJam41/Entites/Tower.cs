﻿using LDJam41.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.Entites
{
    public class Tower : Entity
    {
        public int Level;
        public int DamagePerLevel;

        public Tower(int id) : base(id)
        {
            Level = 1;
            DamagePerLevel = 0;
        }

        public override int InBounds(Vector2 vector)
        {
            Sprite sprite = GetComponent<Sprite>();
            Rectangle bounds = new Rectangle((int)sprite.Position.X, (int)sprite.Position.Y, sprite.Width, sprite.Height);
            if (bounds.Contains(vector))
            {
                return Id;
            }
            return 0;
        }
    }
}
