﻿using LDJam41.Components;
using LDJam41.Entites;
using LDJam41.Entites.Cards;
using LDJam41.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Linq;
using System.Collections.Generic;
using LDJam41.Entites.Towers;
using LDJam41.Entites.Monsters;

namespace LDJam41
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Main : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private Camera camera;
        public Gameboard Gameboard;
        private EntityManager entityManager;
        private List<Wave> waves;
        public SpriteFont font;
        private Vector2 statusPosition;
        private Vector2 centerPosition;

        public int Wave;
        public int Lives;
        public int Energy;
        public int MaxEnergy;
        public List<Card> Hand;
        public List<Card> DrawPile;
        public List<Card> PlayedPile;
        public Card SelectedCard;
        public GameState state;
        public List<Card> Rewards;

        private Vector2 Pointer;
        private MouseState mouseState;
        private MouseState previousMouseState;
        private KeyboardState keyboardState;
        private KeyboardState previousKeyboardState;
        private int previousScrollValue;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            //graphics.PreferredBackBufferWidth = 1280;
            //graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 512;
            graphics.PreferredBackBufferHeight = 644;   // 512 + 96 extra width for cards.
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            camera = new Camera(GraphicsDevice.Viewport);
            Gameboard = new Gameboard(this, "tiles");
            entityManager = new EntityManager(this);
            waves = new List<Wave>();
            DrawPile = new List<Card>();
            PlayedPile = new List<Card>();
            Hand = new List<Card>();
            Rewards = new List<Card>();
            Lives = 100;
            Energy = 0;
            MaxEnergy = 4;
            Wave = 0;
            state = GameState.Setup;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {  
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("Lato-Regular");

            Vector2 fontSize = font.MeasureString("Energy: " + Energy + ", Lives: " + Lives.ToString() + ", Wave: " + (Wave + 1));
            Vector2 center = new Vector2(0 + 512 / 2, 0);
            Vector2 textCenter = new Vector2(fontSize.X / 2, 0);
            statusPosition = new Vector2((int)(center.X - textCenter.X), (int)(center.Y - textCenter.Y));

            fontSize = font.MeasureString("Gameover, R for Retry!");
            center = new Vector2(512 / 2, 640 / 2);
            textCenter = new Vector2(fontSize.X / 2, fontSize.Y / 2);
            centerPosition = new Vector2((int)(center.X - textCenter.X), (int)(center.Y - textCenter.Y));

            Setup();
            StartTurn();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public void Setup()
        {
            Gameboard.PrepareGameboard();

            Lives = 100;
            Energy = 0;
            MaxEnergy = 4;
            Wave = 0;

            DrawPile.Clear();
            PlayedPile.Clear();
            Hand.Clear();
            Rewards.Clear();
            entityManager.Entities.Clear();
            entityManager.EntityCounter = 1;

            entityManager.AddButton(new Vector2(448, 480));

            waves.Add(new Wave(10, 35, 1, new List<Type> { typeof(BasicMonster) }, false));
            waves.Add(new Wave(20, 35, 1, new List<Type> { typeof(FastMonster) }, false));
            waves.Add(new Wave(25, 20, 1, new List<Type> { typeof(StatusImmuneMonster) }, false));
            waves.Add(new Wave(30, 15, 1, new List<Type> { typeof(ArmorMonster) }, false));
            waves.Add(new Wave(1, 0, 1, new List<Type> { typeof(BasicBoss) }, true));
            waves.Add(new Wave(50, 25, 2, new List<Type> { typeof(BasicMonster), typeof(FastMonster) }, false));
            waves.Add(new Wave(50, 20, 2, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster) }, false));
            waves.Add(new Wave(75, 20, 2, new List<Type> { typeof(ArmorMonster), typeof(FastMonster) }, false));
            waves.Add(new Wave(85, 15, 2, new List<Type> { typeof(StatusImmuneMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(1, 0, 2, new List<Type> { typeof(FastBoss) }, true));
            waves.Add(new Wave(100, 20, 3, new List<Type> { typeof(BasicMonster), typeof(FastMonster), typeof(ArmorMonster) }, false));
            waves.Add(new Wave(120, 15, 3, new List<Type> { typeof(StatusImmuneMonster), typeof(BasicMonster), typeof(ArmorMonster) }, false));
            waves.Add(new Wave(150, 15, 3, new List<Type> { typeof(BasicMonster), typeof(ArmorMonster), typeof(FastMonster) }, false));
            waves.Add(new Wave(200, 15, 3, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(1, 0, 3, new List<Type> { typeof(ArmorBoss) }, true));
            waves.Add(new Wave(225, 15, 4, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(225, 10, 4, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(250, 10, 4, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(250, 10, 4, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(1, 0, 4, new List<Type> { typeof(StatusImmuneBoss) }, true));
            waves.Add(new Wave(250, 10, 5, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(260, 10, 5, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(280, 10, 5, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(300, 10, 5, new List<Type> { typeof(StatusImmuneMonster), typeof(ArmorMonster), typeof(FastMonster), typeof(BasicMonster) }, false));
            waves.Add(new Wave(25, 40, 5, new List<Type> { typeof(FastBoss), typeof(StatusImmuneBoss), typeof(ArmorBoss), typeof(FastBoss) }, true));

            DrawPile.Add(new CardBasicTower(this, entityManager.EntityCounter++));
            DrawPile.Add(new CardBasicTower(this, entityManager.EntityCounter++));
            DrawPile.Add(new CardBasicTower(this, entityManager.EntityCounter++));
            DrawPile.Add(new CardUpgrade(this, entityManager.EntityCounter++));
            DrawPile.Add(new CardCannonTower(this, entityManager.EntityCounter++));

            StartTurn();
        }

        public void EndWave(bool boss)
        {
            if (state != GameState.Gameover && state != GameState.Victory)
            {
                if (Wave == waves.Count - 1 && entityManager.MonstersAlive == 0 && entityManager.WaveMonstersLeft == 0)
                {
                    state = GameState.Victory;
                }
                else
                {
                    if(boss)
                    {
                        MaxEnergy = MaxEnergy + 1;
                    }
                    EndTurn();
                }
            }
        }

        public void StartTurn()
        {
            state = GameState.Setup;
            Energy = MaxEnergy;
            Random rng = new Random();
            for (int i = 0; i < 5; i++)
            {
                if (DrawPile.Count > 0)
                {
                    Card card = DrawPile[0];
                    Hand.Add(card);
                    card.AddComponent(new Sprite(card, this, card.Texture, new Vector2((64 * i) + (4 * i + 4), 512), 64, 128, 0f));
                    card.GetComponent<Sprite>().Order = 0.2f;
                    entityManager.AddCard(card);
                    DrawPile.RemoveAt(0);
                }
                else
                {
                    if (PlayedPile.Count > 0)
                    {
                        DrawPile.Clear();
                        DrawPile = PlayedPile.OrderBy(item => rng.Next()).ToList();
                        PlayedPile.Clear();
                        Card card = DrawPile[0];
                        Hand.Add(card);
                        card.AddComponent(new Sprite(card, this, card.Texture, new Vector2((64 * i) + (4 * i + 4), 512), 64, 128, 0f));
                        card.GetComponent<Sprite>().Order = 0.2f;
                        entityManager.AddCard(card);
                        DrawPile.RemoveAt(0);
                    }
                }
            }
        }

        public void StartWave()
        {
            state = GameState.Wave;
            foreach (Card c in Hand)
            {
                c.RemoveComponent(c.GetComponent<Sprite>());
                entityManager.RemoveEntity(c);
                PlayedPile.Add(c);
            }
            Hand.Clear();
            entityManager.AddMonsterWave(waves[Wave]);
        }

        public void EndTurn()
        {
            state = GameState.Reward;
            Random rng = new Random();
            for (int i = 0; i < 3; i++)
            {
                int rarity = rng.Next(1, 100);
                if (rarity >= 75)
                {
                    int cardNr = rng.Next(1, 4);
                    Card card;
                    switch (cardNr)
                    {
                        case 1:
                            card = new CardThunderTower(this, entityManager.EntityCounter++);
                            card.AddComponent(new Sprite(card, this, card.Texture, new Vector2((i + 1) * 115, 192), 64, 128, 0f));
                            card.GetComponent<Sprite>().Order = 0.2f;
                            Rewards.Add(card);
                            entityManager.AddCard(card);
                            break;
                        case 2:
                            card = new CardUpgradePlus(this, entityManager.EntityCounter++);
                            card.AddComponent(new Sprite(card, this, card.Texture, new Vector2((i + 1) * 115, 192), 64, 128, 0f));
                            card.GetComponent<Sprite>().Order = 0.2f;
                            Rewards.Add(card);
                            entityManager.AddCard(card);
                            break;
                        case 3:
                            card = new CardFlameTower(this, entityManager.EntityCounter++);
                            card.AddComponent(new Sprite(card, this, card.Texture, new Vector2((i + 1) * 115, 192), 64, 128, 0f));
                            card.GetComponent<Sprite>().Order = 0.2f;
                            Rewards.Add(card);
                            entityManager.AddCard(card);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    int cardNr = rng.Next(1, 4);
                    Card card;
                    switch (cardNr)
                    {
                        case 1:
                            card = new CardBasicTower(this, entityManager.EntityCounter++);
                            card.AddComponent(new Sprite(card, this, card.Texture, new Vector2((i + 1) * 115, 192), 64, 128, 0f));
                            card.GetComponent<Sprite>().Order = 0.2f;
                            Rewards.Add(card);
                            entityManager.AddCard(card);
                            break;
                        case 2:
                            card = new CardCannonTower(this, entityManager.EntityCounter++);
                            card.AddComponent(new Sprite(card, this, card.Texture, new Vector2((i + 1) * 115, 192), 64, 128, 0f));
                            card.GetComponent<Sprite>().Order = 0.2f;
                            Rewards.Add(card);
                            entityManager.AddCard(card);
                            break;
                        case 3:
                            card = new CardUpgrade(this, entityManager.EntityCounter++);
                            card.AddComponent(new Sprite(card, this, card.Texture, new Vector2((i + 1) * 115, 192), 64, 128, 0f));
                            card.GetComponent<Sprite>().Order = 0.2f;
                            Rewards.Add(card);
                            entityManager.AddCard(card);
                            break;
                        default:
                            break;
                    }
                }
                
            }
        }

        private bool IsMouseInsideBounds(Vector2 vector)
        {
            return camera.viewport.Bounds.Contains(vector);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if(Lives <= 0)
            {
                state = GameState.Gameover;
            }
                
            mouseState = Mouse.GetState();
            keyboardState = Keyboard.GetState();
            int x = mouseState.X;
            int y = mouseState.Y;
            Pointer = new Vector2(x, y);
            Point point = new Point((int)Pointer.X / 32, (int)Pointer.Y / 32);

            if (IsMouseInsideBounds(Pointer) && IsActive)
            {
                if (mouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
                {
                    if (SelectedCard != null)
                    {
                        int id = entityManager.InBounds(Pointer);
                        if (id > 0)
                        {
                            Entity entity = entityManager.GetEntityById(id);
                            if (typeof(Card).IsAssignableFrom(entity.GetType()))
                            {
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                                SelectedCard = null;
                                SelectedCard = (Card)entity;
                                SelectedCard.AddComponent(new Outline(entity, GraphicsDevice));
                            }
                        }
                        if (typeof(CardBasicTower).Equals(SelectedCard.GetType()))
                        {
                            if (Gameboard.IsBuildable(point))
                            {
                                BasicTower tower = new BasicTower(this, entityManager.EntityCounter++, point);
                                entityManager.AddTower(tower);
                                Hand.Remove(SelectedCard);
                                PlayedPile.Add(SelectedCard);
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Sprite>());
                                entityManager.Entities.Remove(SelectedCard);
                                Energy -= SelectedCard.Cost;
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                                SelectedCard = null;
                                Gameboard.SetUnbuildable(point);
                            }
                        }
                        else if (typeof(CardUpgrade).Equals(SelectedCard.GetType()))
                        {
                            id = entityManager.InBounds(Pointer);
                            if (id > 0)
                            {
                                Entity entity = entityManager.GetEntityById(id);
                                if (typeof(Tower).IsAssignableFrom(entity.GetType()))
                                {
                                    Tower tower = (Tower)entity;
                                    tower.Level++;
                                    Hand.Remove(SelectedCard);
                                    PlayedPile.Add(SelectedCard);
                                    SelectedCard.RemoveComponent(SelectedCard.GetComponent<Sprite>());
                                    entityManager.Entities.Remove(SelectedCard);
                                    Energy -= SelectedCard.Cost;
                                    SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                                    SelectedCard = null;
                                }
                            }
                        }
                        else if (typeof(CardUpgradePlus).Equals(SelectedCard.GetType()))
                        {
                            id = entityManager.InBounds(Pointer);
                            if (id > 0)
                            {
                                Entity entity = entityManager.GetEntityById(id);
                                if (typeof(Tower).IsAssignableFrom(entity.GetType()))
                                {
                                    Tower tower = (Tower)entity;
                                    tower.Level = tower.Level + 2;
                                    Hand.Remove(SelectedCard);
                                    PlayedPile.Add(SelectedCard);
                                    SelectedCard.RemoveComponent(SelectedCard.GetComponent<Sprite>());
                                    entityManager.Entities.Remove(SelectedCard);
                                    Energy -= SelectedCard.Cost;
                                    SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                                    SelectedCard = null;
                                }
                            }
                        }
                        else if (typeof(CardCannonTower).Equals(SelectedCard.GetType()))
                        {
                            if (Gameboard.IsBuildable(point))
                            {
                                CannonTower tower = new CannonTower(this, entityManager.EntityCounter++, point);
                                entityManager.AddTower(tower);
                                Hand.Remove(SelectedCard);
                                PlayedPile.Add(SelectedCard);
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Sprite>());
                                entityManager.Entities.Remove(SelectedCard);
                                Energy -= SelectedCard.Cost;
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                                SelectedCard = null;
                                Gameboard.SetUnbuildable(point);
                            }
                        }
                        else if (typeof(CardThunderTower).Equals(SelectedCard.GetType()))
                        {
                            if (Gameboard.IsBuildable(point))
                            {
                                ThunderTower tower = new ThunderTower(this, entityManager.EntityCounter++, point);
                                entityManager.AddTower(tower);
                                Hand.Remove(SelectedCard);
                                PlayedPile.Add(SelectedCard);
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Sprite>());
                                entityManager.Entities.Remove(SelectedCard);
                                Energy -= SelectedCard.Cost;
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                                SelectedCard = null;
                                Gameboard.SetUnbuildable(point);
                            }
                        }
                        else if (typeof(CardFlameTower).Equals(SelectedCard.GetType()))
                        {
                            if (Gameboard.IsBuildable(point))
                            {
                                FlameTower tower = new FlameTower(this, entityManager.EntityCounter++, point);
                                entityManager.AddTower(tower);
                                Hand.Remove(SelectedCard);
                                PlayedPile.Add(SelectedCard);
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Sprite>());
                                entityManager.Entities.Remove(SelectedCard);
                                Energy -= SelectedCard.Cost;
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                                SelectedCard = null;
                                Gameboard.SetUnbuildable(point);
                            }
                        }
                        else if (typeof(CardFrostTower).Equals(SelectedCard.GetType()))
                        {
                            if (Gameboard.IsBuildable(point))
                            {
                                FrostTower tower = new FrostTower(this, entityManager.EntityCounter++, point);
                                entityManager.AddTower(tower);
                                Hand.Remove(SelectedCard);
                                PlayedPile.Add(SelectedCard);
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Sprite>());
                                entityManager.Entities.Remove(SelectedCard);
                                Energy -= SelectedCard.Cost;
                                SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                                SelectedCard = null;
                                Gameboard.SetUnbuildable(point);
                            }
                        }
                    }
                    else
                    {
                        int id = entityManager.InBoundsIgnoreType(Pointer, typeof(Tower));
                        if (id > 0)
                        {
                            Entity entity = entityManager.GetEntityById(id);
                            if (typeof(Card).IsAssignableFrom(entity.GetType()))
                            {
                                System.Diagnostics.Debug.WriteLine(state);

                                Card card = (Card)entity;
                                if (state == GameState.Setup)
                                {
                                    if (card.Cost <= Energy)
                                    {
                                        SelectedCard = (Card)entity;
                                        SelectedCard.AddComponent(new Outline(entity, GraphicsDevice));
                                    }
                                }
                                else if (state == GameState.Reward)
                                {
                                    foreach (Card c in Rewards)
                                    {
                                        c.RemoveComponent(c.GetComponent<Sprite>());
                                        entityManager.RemoveEntity(c);
                                    }
                                    DrawPile.Add(card);
                                    Rewards.Clear();
                                    StartTurn();
                                }
                            }
                            else if (typeof(Button).IsAssignableFrom(entity.GetType()))
                            {
                                if (state == GameState.Setup)
                                {
                                    StartWave();
                                }
                            }
                        }
                    }
                }

                if (mouseState.RightButton == ButtonState.Pressed && previousMouseState.RightButton == ButtonState.Released)
                {
                    if (SelectedCard != null)
                    {
                        SelectedCard.RemoveComponent(SelectedCard.GetComponent<Outline>());
                        SelectedCard = null;
                    }
                }

                if (keyboardState.IsKeyDown(Keys.R) && previousKeyboardState.IsKeyUp(Keys.R))
                {
                    if(state == GameState.Gameover || state == GameState.Victory)
                    {
                        Setup();
                    }
                }
            }

            previousMouseState = mouseState;
            previousKeyboardState = keyboardState;
            previousScrollValue = mouseState.ScrollWheelValue;

            float delta = gameTime.ElapsedGameTime.Milliseconds;
            Gameboard.Update(delta);
            entityManager.Update(delta);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gray);
            spriteBatch.Begin(
                SpriteSortMode.BackToFront,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                null,
                null,
                null,
                camera.ViewMatrix());
            Gameboard.Draw(spriteBatch);
            spriteBatch.DrawString(font, "Energy: " + Energy + ", Lives: " + Lives.ToString() + ", Wave: " + (Wave + 1), statusPosition, Color.Black);
            if(state == GameState.Gameover)
            {
                spriteBatch.DrawString(font, "Gameover, R for retry!", centerPosition, Color.Black);
            }
            else if(state == GameState.Victory)
            {
                spriteBatch.DrawString(font, "Victory, R for new game!", centerPosition, Color.Black);
            }
            entityManager.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
