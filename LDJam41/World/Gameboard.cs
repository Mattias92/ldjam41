﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.World
{
    public class Gameboard
    {
        private readonly Main main;
        private int width;
        private int height;
        private int tileWidth;
        private int tileHeight;
        private Texture2D textureAtlas;
        private Tile[,] tiles;

        public List<Tile> Path { get; set; }

        public Gameboard(Main main, string textureAtlas)
        {
            this.main = main;
            width = 16;
            height = 16;
            tileWidth = 32;
            tileHeight = 32;
            this.textureAtlas = main.Content.Load<Texture2D>(textureAtlas);
            tiles = new Tile[width, height];
            PrepareGameboard();
        }

        public void PrepareGameboard()
        {
            Path = new List<Tile>
            {
                new Tile(2, -1, tileWidth, tileHeight, 1, 0, "Path", false),
                //Start
                new Tile(2, 0, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 1, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 2, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 3, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 4, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 5, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 6, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 7, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 8, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 9, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(2, 10, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(3, 10, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(4, 10, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(5, 10, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(6, 10, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(6, 9, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(6, 8, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(6, 7, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(7, 7, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(8, 7, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 7, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 8, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 9, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 10, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 11, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 12, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 13, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(10, 13, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(11, 13, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(12, 13, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 13, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 12, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 11, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 10, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 9, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 8, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 7, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 6, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 5, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(13, 4, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(12, 4, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(11, 4, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(10, 4, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 4, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 3, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 2, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 1, tileWidth, tileHeight, 1, 0, "Path", false),
                new Tile(9, 0, tileWidth, tileHeight, 1, 0, "Path", false),
                //End
                new Tile(9, -1, tileWidth, tileHeight, 1, 0, "Path", false)
            };

            for (int x = 0; x < width; x++)
            {
                for(int y = 0; y < height; y++)
                {
                    Tile tile = new Tile(x, y, tileWidth, tileHeight, 0, 0, "Grass", true);

                    if (Path.Contains(tile))
                    {
                        tiles[x, y] = Path.First(i => i.X == x && i.Y == y);
                    }
                    else
                    {
                        tiles[x, y] = tile;
                    }
                }
            }
        }

        public bool IsBuildable(Point point)
        {
            if(point.X >= 0 && point.X <= 15)
            {
                if(point.Y >= 0 && point.Y <= 15)
                {
                    return tiles[point.X, point.Y].IsBuildable;
                }
            }
            return false;
        }

        public void SetUnbuildable(Point point)
        {
            if (point.X >= 0 && point.X <= 15)
            {
                if (point.Y >= 0 && point.Y <= 15)
                {
                    tiles[point.X, point.Y].IsBuildable = false;
                }
            }
        }

        public void Update(float delta)
        {

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach(Tile t in tiles)
            {
                t.Draw(spriteBatch, textureAtlas);
            }
        }
    }
}
