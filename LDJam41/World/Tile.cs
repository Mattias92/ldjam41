﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41.World
{
    public class Tile
    {
        public int X                { get; set; }
        public int Y                { get; set; }
        public int Width            { get; set; }
        public int Height           { get; set; }
        public int U                { get; set; }
        public int V                { get; set; }
        public string Name          { get; set; }
        public bool IsBuildable     { get; set; }

        public Tile(int x, int y, int width, int height, int u, int v, string name, bool isBuildable)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            U = u;
            V = v;
            Name = name;
            IsBuildable = isBuildable;
        }

        public override Boolean Equals(System.Object obj)
        {
            var t = obj as Tile;
            if (obj == null)
            {
                return false;
            }
            return X.Equals(t.X) && Y.Equals(t.Y);
        }

        public override int GetHashCode()
        {
            return (((X << Width) | (Y >> Height)) ^ Y).GetHashCode();
        }

        public Point ToPoint()
        {
            return new Point(X, Y);
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X * Width, Y * Height);
        }

        public void Update(float delta)
        {

        }

        public void Draw(SpriteBatch spriteBatch, Texture2D texture)
        {
            spriteBatch.Draw(texture, new Rectangle(X * Width, Y * Height, Width, Height), new Rectangle(U * Height, V * Width, Width, Height), Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
        }
    }
}
