﻿using LDJam41.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam41
{
    public class Entity : IEntity
    {
        public int Id                       { get; set; }
        public List<Component> Components   { get; set; }

        public Entity(int id)
        {
            Id = id;
            Components = new List<Component>();
        }

        public void AddComponent(Component component)
        {
            Components.Add(component);
        }

        public void AddComponent<T>(Component component) where T : Component
        {
            if (!HasComponent<T>())
            {
                Components.Add(component);
            }
        }

        public T GetComponent<T>() where T : Component
        {
            foreach (Component c in Components)
            {
                if (typeof(T).IsAssignableFrom(c.GetType()))
                {
                    return (T)c;
                }
            }
            return null;
        }

        public bool HasComponent<T>() where T : Component
        {
            return GetComponent<T>() != null;
        }

        public void RemoveComponent(Component component)
        {
            Components.Remove(component);
        }

        public virtual int InBounds(Vector2 vector)
        {
            return 0;
        }

        public virtual int InBoundsIgnoreType(Vector2 vector, Type type)
        {
            return 0;
        }

        public virtual void Update(float delta)
        {
            for (int i = 0; i < Components.Count; i++)
            {
                if (Components[i].IsKilled)
                {
                    Components[i].Kill();
                }
                else
                {
                    Components[i].Update(delta);
                }
            }
        }

        public virtual void Update(float delta, EntityManager entityManager)
        {
            for (int i = 0; i < Components.Count; i++)
            {
                if (Components[i].IsKilled)
                {
                    Components[i].Kill();
                }
                else
                {
                    Components[i].Update(delta, entityManager);
                }
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach(Component c in Components)
            {
                c.Draw(spriteBatch);
            }
        }
    }
}
