﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace LDJam41
{
    public class Camera
    {
        public readonly Viewport viewport;
        public Vector2 Postion { get; private set; }
        public Vector2 Origin { get; private set; }
        public float Zoom { get; private set; }
        public float Rotation { get; private set; }
        private float minZoom;
        private float maxZoom;

        public Camera(Viewport viewport)
        {
            this.viewport = viewport;
            Postion = Vector2.Zero;
            Origin = new Vector2(viewport.Width / 2f, viewport.Height / 2f);
            Zoom = 1f;
            Rotation = 0f;
            minZoom = 0.5f;
            maxZoom = 2f;
        }

        public Matrix ViewMatrix()
        {
            return
                Matrix.CreateTranslation(new Vector3(-Postion, 0f)) *
                Matrix.CreateTranslation(new Vector3(-Origin, 0.0f)) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateScale(Zoom, Zoom, 1) *
                Matrix.CreateTranslation(new Vector3(Origin, 0.0f));
        }

        public void SetZoom(float amount)
        {
            Zoom += amount;
            if (Zoom > maxZoom)
                Zoom = maxZoom;
            else if (Zoom < minZoom)
                Zoom = minZoom;
        }

        public void MovePosition(Vector2 direction)
        {
            Postion += Vector2.Transform(direction, Matrix.CreateRotationZ(-Rotation));
        }

        public Point ScreenToPoint(float x, float y)
        {
            Vector2 pos = ScreenToPoint(new Vector2(x, y));
            return new Point((int)Math.Floor(pos.X / 16), (int)Math.Floor(pos.Y / 16));
        }

        public Vector2 ScreenToPoint(Vector2 position)
        {
            return Vector2.Transform(position - new Vector2(viewport.X, viewport.Y), Matrix.Invert(ViewMatrix()));
        }
    }
}
